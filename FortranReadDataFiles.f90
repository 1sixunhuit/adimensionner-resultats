 program test_read

   implicit none
   integer, parameter :: VLI_K = selected_int_kind (18)
   integer, parameter :: DR_K = selected_real_kind (14)
   
   integer (VLI_K) :: i1
   real (DR_K) :: i, a, b, c, d, e

   real(dr_k), allocatable, dimension(:) :: T  ;
   real(dr_k), allocatable, dimension(:) :: P  ;
   real(dr_k), allocatable, dimension(:) :: T1 ;
   real(dr_k), allocatable, dimension(:) :: P1 ;
   real(dr_k), allocatable, dimension(:) :: R  ;
   real(dr_k), allocatable, dimension(:) :: He  ; ! hauteur eau

   integer, parameter :: N = 173832 ;
   
   real(dr_k), parameter :: rho = 1e3 ;
   real(dr_k), parameter :: p0 = 1e5
   real(dr_k), parameter :: g = 9.81;
   real(dr_k), parameter ::H = 0.3d0;
   CHARACTER*1 :: CHARI1;
   integer(kind=4)::fn;
   ALLOCATE(P(1:N)) ;
   ALLOCATE(P1(1:N)) ;
   ALLOCATE(T(1:N)) ;   ALLOCATE(R(1:N)) ;    ALLOCATE(He(1:N)) ;
   ALLOCATE(T1(1:N));
   
   do fn = 1,8
      open (unit=15, file="sonde"//CHARI1(fn)//"_result.dat", status='old',    &
             access='sequential', form='formatted', action='read' )

   
      !110 format (F6.5, 2 (1X, D5.4) ) ! lecture
110   format ((6e13.5))
120   format ((6e13.5))
      !120 format (F6.5, 2 (2X, D12.3) )    ! ecriture
      !   120 format (F6.5, D12.3, D12.3 )
      
      DO i1 = 1, N
         read (15, 110)  i, a, b, c, d, e
         !      write (*, 120) i, a, b, c, d, e
         T(i1) = i ; P(i1) = e ; R(i1) = a ;
      ENDDO
      !   read (15, *) i, a, b
      !  write (*, 120) i, a, b, c, d, e
      
      
      do i1 = 1, N
         P1(i1) = (P(i1)- p0) / (rho * g * H) ;
         He(i1) = (P(i1)-p0) / (rho * g) ;
         T1(i1) = T(i1) * dsqrt(g/H) ;
         !print*,P1(i1)
      end do
      
      CALL SYSTEM("mkdir -p adim_files")
      open(unit=16, file="adimensionnalsond_"//CHARI1(fn)//".dat")
      write(16,*) "# T*  P*  Heau  Rho"
      DO i1 = 1, N
         write(16, 120) T1(i1), P1(i1), He(i1), R(i1)
      ENDDO
      close(16)
   END DO
   CALL SYSTEM("mv adimensionnalsond_* ./adim_files")
   DEALLOCATE(T, T1, P, P1, He, R)
   
   CALL SYSTEM("gnuplot 'gnuplot.plt'")
 end program test_read

 CHARACTER*1 FUNCTION CHARI1(M)
!
!  Converts integer M into a one character string.  If M does not fit
!  in one character, * is inserted instead.
!
!  ---Input---
      INTEGER ::  M
!
!  This file also contains functions CHARIn, n=2,3,...,10, which
!  produce strings of length n.
!==============================================================================
!  1990 Nov  1  Written by Matthew Nicoll, Cypher Consulting
! http://computer-programming-forum.com/49-fortran/c499b11b42ebb51b.htm
!******************************************************************************

      WRITE(CHARI1,'(I1)',ERR=9)M
  9   RETURN
      END

