#Temps depart, temps fin
td = 2.2
tf = 18

set term png size 400,200 font "/usr/share/fonts/truetype/liberation/LiberationSans-Regular.ttf,6" ;
#set term png size 800,600 font "/usr/share/fonts/truetype/liberation/LiberationSans-Regular.ttf,6" ;

#On a 2(= j)x4(=i) sondes
do for [j=1:2]{
do for [c=2:4]{
  if (c == 2){ set output "pressionsadim".j.".png" ;  set title "Pression adimensionnée"  ; }
  if (c == 3){ set output "hauteurcalc".j.".png"   ;  set title "Hauteur calculée"        ; }
  if (c == 4){ set output "liquide".j.".png"       ;  set title "Fluide présent"          ; }
  
  plot [td:tf][:] for [i=1:4]  'adim_files/adimensionnalsond_'.(4*(j-1)+i).'.dat'  u 1:c w l title "sonde ".(4*(j-1)+i)
}
}
